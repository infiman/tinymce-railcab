# coding: utf-8
lib = File.expand_path('../lib', __FILE__)

$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)

require 'tinymce-railcab/version'

Gem::Specification.new do |spec|
  spec.name     = 'tinymce-railcab'
  spec.version  = TinyMCE::VERSION
  spec.authors  = ['Alexey Chernetsov']
  spec.email    = ['chernetsov0@gmail.com']
  spec.summary  = 'Rails integration for TinyMCE - a platform independent web based Javascript HTML WYSIWYG editor control released as Open Source under LGPL.'
  spec.homepage = 'https://github.com/railcabs/tinymce-railcab'
  spec.license  = 'LGPL'
  spec.files    = `git ls-files`.split("\n")
  # spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler'   , '~> 1.7'
  spec.add_development_dependency 'rake'      , '~> 10.0'
  spec.add_development_dependency 'versionomy', '~> 0.4.4'
end

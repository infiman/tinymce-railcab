# TinyMCE Railcab

Rails integration for TinyMCE - a platform independent web based Javascript HTML WYSIWYG editor control released as Open Source under LGPL.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'tinymce-railcab'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install tinymce-railcab

## Contributing

1. Fork it ( https://github.com/tinymce-railcab/tinymce-railcab/fork )
2. Create your feature branch (`git checkout -b feature-super-important`)
3. Commit your changes (`git commit -am 'Add super important feature'`)
4. Push to the branch (`git push origin feature-super-important`)
5. Create a new Pull Request

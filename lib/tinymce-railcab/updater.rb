require 'rubygems/package'
require 'net/https'
require 'open-uri'
require 'json'
require 'uri'
require 'zlib'

require 'versionomy'

module TinyMCE
  class Updater
    TAGS_URI = 'https://api.github.com/repos/tinymce/tinymce-dist/tags'

    ARCHIVE = 'tmp/tinymce.tgz'
    FOLDER  = 'tmp/tinymce'

    DESTINATION_JS     = 'vendor/assets/javascripts'
    DESTINATION_CSS    = 'vendor/assets/stylesheets'
    DESTINATION_FONTS  = 'vendor/assets/fonts/tinymce'
    DESTINATION_IMAGES = 'vendor/assets/images/tinymce'

    def self.update!
      self.new.update!
    end

    def update!
      latest = JSON::load(open TAGS_URI)[1]

      latest_version = latest['name']
      latest_tarball = latest['tarball_url']

      if Versionomy.parse(latest_version) <= Versionomy.parse(VERSION)
        puts 'Already up to date.'
        return
      end

      download latest_tarball and unpack

      copy_core
      copy_plugins
      copy_assets

      update latest_version
    end

    def download archive
      puts '-> Downloading archive'

      FileUtils.mkdir_p 'tmp'

      File.open(ARCHIVE, 'w') do |file|
        file.write open(archive).read
      end
    end

    def unpack
      puts '-> Unpacking archive'

      Gem::Package::TarReader.new Zlib::GzipReader.open(ARCHIVE) do |tar|
        destination = nil

        tar.each do |entry|
          destination = File.join FOLDER, entry.full_name.split('/').drop(1).join('/')

          if entry.directory?
            FileUtils.rm_rf   destination unless File.directory? destination
            FileUtils.mkdir_p destination, mode: entry.header.mode, verbose: false
          elsif entry.file?
            FileUtils.rm_rf destination unless File.file? destination
            File.open(destination, "wb") { |f| f.print entry.read }
            FileUtils.chmod entry.header.mode, destination, verbose: false
          end
        end
      end
    end

    def copy_core
      puts '-> Copying core'

      FileUtils.mkdir_p File.join(DESTINATION_JS    , 'tinymce')
      FileUtils.mkdir_p File.join(DESTINATION_CSS   )
      FileUtils.mkdir_p File.join(DESTINATION_FONTS )
      FileUtils.mkdir_p File.join(DESTINATION_IMAGES)

      puts '   # core'
      FileUtils.copy File.join(FOLDER, 'tinymce.js'), File.join(DESTINATION_JS, 'tinymce.js')
      puts '   # core.jquery'
      FileUtils.copy File.join(FOLDER, 'tinymce.jquery.js'), File.join(DESTINATION_JS, 'tinymce-jquery.js')
      puts '   # core.jquery'
      FileUtils.copy File.join(FOLDER, 'jquery.tinymce.min.js'), File.join(DESTINATION_JS, 'tinymce-jquery-plugin.js')

      puts '   # core.theme.modern'
      FileUtils.copy File.join(FOLDER, 'themes', 'modern', 'theme.js'), File.join(DESTINATION_JS, 'tinymce-theme.js')
    end

    def copy_plugins
      puts '-> Copying plugins'

      folder = File.join(FOLDER, "plugins")

      plugins = Dir.glob(folder + '/**/*.js').select { |file|
        file !~ /\.min\.js$/
      }.map { |file|
        file.gsub(folder + '/', '').gsub('/plugin.js', '')
      }

      plugins.each do |plugin|
        puts '   # plugin.' + plugin

        FileUtils.copy(
          File.join(folder, plugin, 'plugin.js'),
          File.join(DESTINATION_JS, 'tinymce', plugin + '.js')
        )
      end
    end

    def copy_assets
      puts '-> Copying assets'

      theme = File.join(FOLDER, 'skins', 'lightgray')

      puts '   # skin.fonts'
      FileUtils.rm_rf DESTINATION_FONTS
      FileUtils.cp_r File.join(theme, 'fonts'), DESTINATION_FONTS
      puts '   # skin.images'
      FileUtils.rm_rf DESTINATION_IMAGES
      FileUtils.cp_r File.join(theme, 'img')  , DESTINATION_IMAGES

      puts '   # skin.styles'
      FileUtils.copy(
        File.join(theme, 'skin.min.css'),
        File.join(DESTINATION_CSS, 'tinymce-skin.scss')
      )
      FileUtils.copy(
        File.join(theme, 'content.min.css'),
        File.join(DESTINATION_CSS, 'tinymce-skin-content.scss')
      )
      FileUtils.copy(
        File.join(theme, 'content.inline.min.css'),
        File.join(DESTINATION_CSS, 'tinymce-skin-inline.scss')
      )

      fix_files DESTINATION_CSS, [
        'tinymce-skin.scss',
        'tinymce-skin-content.scss',
        'tinymce-skin-inline.scss'
      ]
    end

    def update version
      puts '-> Updating version'

      file     = Pathname.new('lib/tinymce-railcab/version.rb')
      content  = file.read
      line     = content.lines.find { |l| l =~ /^\s+VERSION/ }
      new_line = line.gsub(/'[^']+'/, %Q{'#{version}'})

      file.open('w+') { |f| f.write content.gsub(line, new_line) }
    end

    def fix_files root, files
      files.each do |file|
        path    = File.join(root, file)
        content = File.open(path).read

        content.gsub! /url\(fonts\/([a-zA-Z0-9\-\_\.]+)\)/, 'font-url("tinymce/\1")'
        content.gsub! /url\(img\/([a-zA-Z0-9\-\_\.]+)\)/  , 'image-url("tinymce/\1")'

        File.open(path, 'w+') { |f| f.write content }
      end
    end
  end
end

module TinyMCE
  class Engine < ::Rails::Engine
    initializer 'tinymce.assets.precompile' do |app|
      %w(stylesheets javascripts fonts images).each do |sub|
        app.config.assets.paths << root.join('vendor/assets', sub).to_s
      end

      # sprockets-rails 3 tracks down the calls to `font_path` and `image_path`
      # and automatically precompiles the referenced assets.
      unless Sprockets::Rails::VERSION.split('.', 2)[0].to_i >= 3
        app.config.assets.precompile << %r(tinymce/tinymce-small\.(?:eot|svg|ttf|woff2?)$)
        app.config.assets.precompile << %r(tinymce/tinymce\.(?:eot|svg|ttf|woff2?)$)
        app.config.assets.precompile << %r(tinymce/.*\.gif$)
      end
    end
  end
end
